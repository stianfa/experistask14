﻿using System;

namespace Task14
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialization
            int num;
            int largestNum = 0;
            int index1 = 0;
            int index2 = 0;


            // Bruteforce, checks all combinations and keeps track of the largest palindrome
            for (int i = 100; i < 1000; i++)
            {
                for (int j = 100; j < 1000; j++)
                {
                    num = i*j;

                    if(isPalindrome(num))
                    {
                        if(num > largestNum)
                        {
                            largestNum = num;
                            index1 = i;
                            index2 = j;
                        }
                    }
                }
            }

            Console.WriteLine($"Largest palindrome: {largestNum}");
            Console.WriteLine($"Product of {index1} * {index2}");
        }


        // Checks if the given number is a palindrome
        public static bool isPalindrome(int num)
        {
            string digits =num.ToString();

            char[] charArray = digits.ToCharArray();
            Array.Reverse(charArray);
            string reverse = new string(charArray);

            return reverse.Equals(digits);  
        }
    }
}
